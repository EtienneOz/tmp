import fontforge
import os
import glob

pathIn = "fonts/"
pathOut = "exports/"
files = [f for f in glob.glob(pathIn + "*.ttf", recursive=True)]

for file in files:
    dirName = file.replace(pathIn, '')
    dirName = dirName.replace('.ttf', '')
    fullPath = pathOut + dirName

    try:
        os.mkdir(fullPath)
    except OSError:
        print ("Existing directory: %s " % fullPath)
    else:
        print ("Successfully created the directory %s " % fullPath)

    F = fontforge.open(file)

    for name in F:
        filename = name + ".png"
        F[name].export(fullPath  + "/" + filename,500)
