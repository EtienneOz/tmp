cd exports
for folder in */; do

  echo -e "\n" $folder

  for filePath in $folder*.png; do

    convert $filePath -resize 32x32 \
                      -extent 32x32  $filePath
    read -r grayScale <<< $( convert $filePath -scale 1x1! -format "%[pixel:s.p{0,0}]" info:)

    grayScale=${grayScale#*(}   # remove prefix ending in "_"
    grayScale=${grayScale%)*}   # remove prefix ending in "_"
    grayScale="num/"$grayScale
    fileName=${filePath#*/}

    echo $fileName

    if [ -d $grayScale ]
    cp $filePath $grayScale"/"$filename
    then
      continue
    else
      mkdir $grayScale
      cp $filePath $grayScale"/"$filename
    fi

  done

done
